# How to add group/projects by gitlab API
- apply a token in "profile settings -> access token"
- add group and record the id (namspace_id)

```bash
curl -X POST -H "PRIVATE-TOKEN: V8Vwxhy86w9Mg7tBXy7H" -d name="g-1" -d path="g-1" "https://gitlab.com/api/v3/groups" 
```

- add a project "p1" to group 

```bash
curl -X POST -H "PRIVATE-TOKEN: V8Vwxhy86w9Mg7tBXy7H" -d name="p1" -d namespace_id="998244" "https://gitlab.com/api/v3/projects"
```

# Create django project

```bash
docker run -it --rm --user "$(id -u):$(id -g)" -v "$PWD":/usr/src/app -w /usr/src/app daocloud.io/library/django django-admin.py startproject pycon
```

# Add requirements.txt

# Start
docker-compose up